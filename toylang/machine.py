from __future__ import annotations

import enum
import logging
from typing import ClassVar

from toylang.isa import ArgType, Instruction, Opcode

# DataPath


class LogicError(Exception):
    def __init__(self, message: str) -> None:
        super().__init__(message)


class AluOp(enum.Enum):
    ADD = enum.auto()
    SUB = enum.auto()
    MUL = enum.auto()
    DIV = enum.auto()
    REM = enum.auto()
    NEG = enum.auto()

    # bitwise logical operations
    AND = enum.auto()
    OR = enum.auto()

    # logical not
    NOT = enum.auto()

    CMP = enum.auto()

    SETG = enum.auto()
    SETE = enum.auto()
    SETL = enum.auto()

    GETARG = enum.auto()
    GETACC = enum.auto()


class Selector(enum.Enum):
    FROM_DR = enum.auto()
    FROM_PORT = enum.auto()
    FROM_MEMORY = enum.auto()
    FROM_ACC = enum.auto()
    FROM_ALU = enum.auto()
    SEL_0 = enum.auto()
    SEL_INC = enum.auto()
    SEL_DEC = enum.auto()
    FROM_IP = enum.auto()
    FROM_CR = enum.auto()
    FROM_AR = enum.auto()
    FROM_SP = enum.auto()
    FROM_INPUT = enum.auto()
    TO_INPUT = enum.auto()


class ALU:
    def __init__(self):
        self.Z = 0
        self.N = 0
        self.result: int = 0

    def execute(self, operation: Opcode, lhs: int, rhs: int):
        result = 0
        match operation:
            case Opcode.ADD:
                result = lhs + rhs
            case Opcode.SUB:
                result = lhs - rhs
            case Opcode.MUL:
                result = rhs * lhs
            case Opcode.DIV:
                result = lhs // rhs
            case Opcode.MOD:
                result = lhs % rhs
            case Opcode.CMP:
                result = lhs - rhs
            case Opcode.SETE:
                if self.Z == 1 and self.N == 0:
                    result = 1
                else:
                    result = 0
            case Opcode.SETG:
                if self.Z == 0 and self.N == 0:
                    result = 1
                else:
                    result = 0
            case Opcode.SETL:
                if self.Z == 0 and self.N == 1:
                    result = 1
                else:
                    result = 0
            case Opcode.AND:
                result = lhs & rhs
            case Opcode.OR:
                result = lhs | rhs
            case Opcode.NOT:
                if lhs == 0:
                    result = 1
                if lhs != 0:
                    result = 0
            case Opcode.NEG:
                result = -lhs
            case Opcode.REM:
                result = lhs % rhs

        self.result = result
        self.set_flags()

    def set_flags(self, res: int = 0) -> None:
        if res:
            self.Z = 1 if res == 0 else 0
            self.N = 1 if res < 0 else 0
        else:
            self.Z = 1 if self.result == 0 else 0
            self.N = 1 if self.result < 0 else 0

    def get_flags(self) -> list[int]:
        return [self.Z, self.N]


class Device:
    output_buffer: ClassVar[list] = []
    input_buffer: ClassVar[list] = []

    def __init__(self, input_file: str) -> None:
        self.input_char_ptr = -1
        f = open(input_file)
        prog = f.read()
        self.prog = [char for char in prog]
        self.prog.append("\n")

    def output(self, value: int) -> None:
        if value == 0:
            return
        if chr(value) == " ":
            logging.debug("".join(self.output_buffer) + " <- ' '")
        else:
            logging.debug("".join(self.output_buffer) + " <- " + chr(value))
        self.output_buffer.append(chr(value))

    def add_to_output_buffer(self, value):
        self.output_buffer.append(value)

    def out_buffer(self):
        out_buffer_line = ""
        for i in self.output_buffer:
            out_buffer_line += chr(i)
        logging.debug("Output: " + "".join(out_buffer_line))
        print("".join(out_buffer_line))
        self.output_buffer.clear()

    def get_next_char(self):
        self.input_char_ptr += 1
        if self.input_char_ptr >= len(self.prog):
            return 10
        value = ord(self.prog[self.input_char_ptr]) if self.prog[self.input_char_ptr] != "\n" else 10
        if value == 10:
            logging.debug("input -> 10")
        else:
            logging.debug("INPUT -> %str" % (chr(value)))
        if value == 10:
            pass
        return value


class DataPath:
    def __init__(self, memory_size: int, input_file: str) -> None:
        self.memory_size = memory_size
        self.acc = 0
        self.da = 0
        self.dr = 0
        self.in_reg = 0
        self.out_reg = 0
        self.memory = [0] * memory_size
        self.left_op = 0
        self.right_op = 0
        self.device = Device(input_file)
        self.alu = ALU()

    def latch_acc(self, sel: Selector) -> None:
        assert sel in {
            Selector.FROM_ALU,
            Selector.FROM_INPUT,
            Selector.FROM_DR,
        }, f"Selector error,  incorrect selector: %{sel}"
        if sel == Selector.FROM_ALU:
            self.acc = self.alu.result
        if sel == Selector.FROM_INPUT:
            self.acc = self.device.get_next_char()
        if sel == Selector.FROM_DR:
            self.acc = self.dr

    def latch_da(self, sel: Selector, value=0) -> None:
        assert sel in {
            Selector.FROM_CR,
            Selector.FROM_DR,
            Selector.FROM_MEMORY,
        }, f"Selector error, incorrect selector: %{sel}"
        if sel == Selector.FROM_CR:
            self.da = value
        if sel == Selector.FROM_DR:
            self.da = self.dr
        if sel == Selector.FROM_MEMORY:
            self.da = self.memory[self.da]

    def latch_dr(self, sel: Selector, value: int = 0) -> None:
        assert sel in {Selector.FROM_CR, Selector.FROM_MEMORY}, f"Selector error, incorrect selector: {sel}"
        if sel == Selector.FROM_CR:
            self.dr = value
        else:
            self.dr = self.oe(sel)

    def latch_lhs(self) -> None:
        self.left_op = self.acc

    def latch_rhs(self) -> None:
        self.right_op = self.dr

    def wr(self, sel: Selector) -> None:
        assert sel in {Selector.FROM_AR}, f"Selector error, incorrect selector: %{sel}"
        if sel == Selector.FROM_AR:
            self.memory[self.da] = self.acc

    def oe(self, sel: Selector) -> int:
        assert sel in {Selector.FROM_MEMORY}, f"Selector error, incorrect selector: ${sel}"
        if self.da is None:
            return 0
        return self.memory[self.da]

    def alu_start_calc(self, opcode: Opcode) -> None:
        self.alu.execute(opcode, self.left_op, self.right_op)

    def get_flags(self) -> list[int]:
        return self.alu.get_flags()

    def latch_out(self) -> None:
        self.device.add_to_output_buffer(self.acc)


class State(enum.Enum):
    INSTR_FETCH = enum.auto
    SWITCH_AR = enum.auto
    SWITCH_AR_FROM_MEMORY = enum.auto
    EXECUTE = enum.auto


class ControlUnit:
    def __init__(self, code: list[Instruction], data_path: DataPath) -> None:
        self.pc = 0
        self.cr = None
        self.program_memory = code
        self.data_path = data_path
        self.tick_counter = 0

    def latch_pc(self, sel: Selector) -> None:
        if sel == Selector.FROM_IP:
            self.pc += 1
        if sel == Selector.FROM_CR:
            self.pc = self.cr.arg

    def latch_cr(self) -> None:
        self.cr = self.program_memory[self.pc]

    def instr_fetch(self) -> None:
        self.latch_cr()
        self.latch_pc(Selector.FROM_IP)
        self.tick()
        logging.debug("%s", self)

    def operand_fetch(self) -> None:
        if self.cr.arg_type == ArgType.IMMEDIATE:
            self.data_path.latch_dr(Selector.FROM_CR, self.cr.arg)
            self.tick()
        if self.cr.arg_type in [ArgType.DIRECT, ArgType.INDIRECT]:
            self.data_path.latch_da(Selector.FROM_CR, self.cr.arg)
            self.tick()
        if self.cr.arg_type == ArgType.INDIRECT:
            self.data_path.latch_da(Selector.FROM_MEMORY)
            self.tick()
        if self.cr.arg_type in [ArgType.DIRECT, ArgType.INDIRECT] and self.cr.opcode != Opcode.ST:
            self.data_path.latch_dr(Selector.FROM_MEMORY)
            self.tick()

    def execute_command(self) -> None:
        match self.cr.opcode:
            case Opcode.ADD | Opcode.SUB | Opcode.MUL | Opcode.DIV | Opcode.MOD | Opcode.AND | Opcode.OR | Opcode.MOD:
                self.data_path.latch_lhs()
                self.data_path.latch_rhs()
                self.data_path.alu_start_calc(self.cr.opcode)
                self.data_path.latch_acc(Selector.FROM_ALU)
            case Opcode.LD:
                self.data_path.latch_acc(Selector.FROM_DR)
            case Opcode.ST:
                if self.cr.arg_type == ArgType.IMMEDIATE:
                    self.data_path.latch_da(Selector.FROM_DR)
                self.data_path.wr(Selector.FROM_AR)
            case Opcode.OUT:
                self.data_path.latch_out()
            case Opcode.IN:
                self.data_path.latch_acc(Selector.FROM_INPUT)
            case Opcode.JNZ | Opcode.JZ | Opcode.JMP:
                zero, neg = self.data_path.get_flags()
                if (
                    zero == 0
                    and self.cr.opcode == Opcode.JNZ
                    or zero == 1
                    and self.cr.opcode == Opcode.JZ
                    or self.cr.opcode == Opcode.JMP
                ):
                    self.latch_pc(Selector.FROM_CR)
            case Opcode.SETE | Opcode.SETG | Opcode.SETL:
                self.data_path.alu_start_calc(self.cr.opcode)
                self.data_path.latch_acc(Selector.FROM_ALU)
            case Opcode.NEG | Opcode.NOT:
                self.data_path.latch_lhs()
                self.data_path.alu_start_calc(self.cr.opcode)
                self.data_path.latch_acc(Selector.FROM_ALU)
            case Opcode.REM:
                self.data_path.latch_lhs()
                self.data_path.latch_rhs()
                self.data_path.alu_start_calc(self.cr.opcode)
                self.data_path.latch_acc(Selector.FROM_ALU)
            case Opcode.CMP:
                self.data_path.latch_lhs()
                self.data_path.latch_rhs()
                self.data_path.alu_start_calc(Opcode.CMP)

            case Opcode.HALT:
                raise StopIteration()
            case _:
                raise LogicError(message="Incorrect opcode %s" % self.cr.opcode)

    def process_instr(self) -> None:
        self.instr_fetch()
        non_addr_command = [
            Opcode.IN,
            Opcode.SETL,
            Opcode.SETE,
            Opcode.SETG,
            Opcode.OUT,
            Opcode.NOT,
            Opcode.NEG,
            Opcode.HALT,
        ]
        if self.cr.opcode not in non_addr_command:
            self.operand_fetch()
        self.execute_command()

    def tick(self) -> None:
        self.tick_counter += 1

    def __str__(self) -> str:
        return "TICK: {:5} | Command: {:10} | IP {:10} | ACC: {:10} | DR {: 5} | PC: {:3} | DATA_ADDR: {:10}".format(
            self.tick_counter,
            self.cr.opcode,
            self.pc,
            self.data_path.acc,
            self.data_path.dr,
            self.pc,
            self.data_path.da,
        )
