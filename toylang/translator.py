from __future__ import annotations

import enum
import io
import sys
from io import TextIOBase
from itertools import groupby
from types import MappingProxyType

from src.tokenizer import Token, Tokenizer, TokenType

from toylang.isa import WORD_SIZE, ArgType, Instruction, Opcode


@enum.unique
class AstExprType(enum.IntFlag):
    INT = enum.auto()
    CHAR = enum.auto()
    STRING = enum.auto()


class AstVariable:
    def __init__(self, var_type: AstExprType, name: str) -> None:
        self.type = var_type
        self.name = name


class ParseError(Exception):
    "Exception raised for errors during translation"

    def __init__(
        self, token: Token | None = None, expected: TokenType | None = None, message: str | None = None
    ) -> None:
        if message is not None:
            super().__init__(message)
            return
        if token is None and expected is not None:
            super().__init__(f"Failed to parse: no tokens left, expected {expected.name}")
            return
        if token is not None and expected is not None:
            super().__init__(f"Failed to parse: wrong token {token.token_type.name} expected {expected.name}")
            return
        if token is not None and expected is None:
            super().__init__(f"Failed to parse: wrong token {token.token_type.name}")
            return

        super().__init__("Unknown parse error")


class TranslateError(Exception):
    def __init__(self, message: str) -> None:
        super().__init__(f"TranslateError occured: {message}")


class Mark:
    def __init__(self) -> None:
        self.position = None

    def set_position(self, pos) -> None:
        self.position = pos


class MarkedInstr:
    def __init__(self, instr: Instruction, mark: Mark) -> None:
        self.instr = instr
        self.mark = mark


def resolve_marks(instrs: list[Instruction | Mark | MarkedInstr]) -> list[Instruction]:
    mark_cnt = 0
    for pos, i in enumerate(instrs):
        if isinstance(i, Mark):
            i.set_position(pos - mark_cnt)
            mark_cnt += 1

    res: list[Instruction] = []
    for r in filter(lambda x: not isinstance(x, Mark), instrs):
        if isinstance(r, MarkedInstr):
            r.instr.arg = r.mark.position
            res.append(r.instr)
        if isinstance(r, Instruction):
            res.append(r)

    return res  # type: ignore


class Translator:
    def __init__(self) -> None:
        self.output_port = 0x0000001
        self.input_port = 0x0000010

        self.data_start = 0x2000000
        self.data_end = 0x2FFFFFF
        self.free_data = self.data_start

        self.mem_start = 0x3000000
        self.mem_end = 0x3FFFFFF
        self.mem_cur = 0x3000000
        self.mem_free_list: list[int] = []

        self.preload: list[Instruction] = []

        self.var_table: dict[str, int] = {}

    def allocate_data(self, size: int, val: list[int]) -> int:
        assert size >= len(val)

        if self.free_data + size >= self.data_end:
            raise TranslateError(message="Data memory exhausted")
        for i, v in enumerate(val):
            if v < 0 or v >= 2 ** (WORD_SIZE):
                raise TranslateError(message="Store immediate argument bigger than word")
            self.preload.append(Instruction(Opcode.LD, v, ArgType.IMMEDIATE))
            self.preload.append(Instruction(Opcode.ST, self.free_data + i, ArgType.IMMEDIATE))
        res = self.free_data
        self.free_data += size
        return res

    def _allocate_prog_mem(self) -> int:
        if self.mem_free_list:
            return self.mem_free_list.pop()
        if self.mem_cur < self.mem_end:
            self.mem_cur += 1
            return self.mem_cur - 1
        raise TranslateError(message="Program memory exhausted")

    def _free_prog_mem(self, ptr: int) -> None:
        self.mem_free_list.append(ptr)

    def allocate_for_tmp_expr(self) -> int:
        return self._allocate_prog_mem()

    def free_tmp_expr(self, ptr: int) -> None:
        self._free_prog_mem(ptr)

    def allocate_var(self, name: str) -> int:
        addr = self._allocate_prog_mem()
        self.var_table[name] = addr
        return addr

    def get_var_addr(self, name: str) -> int:
        return self.var_table[name]


@enum.unique
class AstOpType(enum.Enum):
    # logical not
    NOT = 5, [([AstExprType.INT], AstExprType.INT)], enum.auto()

    UN_MINUS = 5, [([AstExprType.INT], AstExprType.INT)], enum.auto()

    MUL = 4, [([AstExprType.INT, AstExprType.INT], AstExprType.INT)], enum.auto()
    DIV = 4, [([AstExprType.INT, AstExprType.INT], AstExprType.INT)], enum.auto()
    MOD = 4, [([AstExprType.INT, AstExprType.INT], AstExprType.INT)], enum.auto()

    PLUS = (
        3,
        [
            ([AstExprType.INT, AstExprType.INT], AstExprType.INT),
            ([AstExprType.INT, AstExprType.CHAR], AstExprType.CHAR),
            ([AstExprType.CHAR, AstExprType.INT], AstExprType.CHAR),
        ],
        enum.auto(),
    )
    BIN_MINUS = (
        3,
        [
            ([AstExprType.INT, AstExprType.INT], AstExprType.INT),
            ([AstExprType.CHAR, AstExprType.CHAR], AstExprType.INT),
        ],
        enum.auto(),
    )

    NEQ = (
        2,
        [
            ([AstExprType.INT, AstExprType.INT], AstExprType.INT),
            ([AstExprType.CHAR, AstExprType.CHAR], AstExprType.INT),
        ],
        enum.auto(),
    )
    EQ = (
        2,
        [
            ([AstExprType.INT, AstExprType.INT], AstExprType.INT),
            ([AstExprType.CHAR, AstExprType.CHAR], AstExprType.INT),
        ],
        enum.auto(),
    )
    LT = 2, [([AstExprType.INT, AstExprType.INT], AstExprType.INT)], enum.auto()
    GT = 2, [([AstExprType.INT, AstExprType.INT], AstExprType.INT)], enum.auto()

    # bitwise logical operations
    OR = 1, [([AstExprType.INT, AstExprType.INT], AstExprType.INT)], enum.auto()
    AND = 1, [([AstExprType.INT, AstExprType.INT], AstExprType.INT)], enum.auto()

    BRACKETS = -1, [([AstExprType.STRING, AstExprType.INT], AstExprType.CHAR)], enum.auto()

    L_PAR = -1, [], enum.auto()  # Special value to parse

    def __init__(self, priority: int, signatures: list[tuple[list[AstExprType], AstExprType]], num: int) -> None:
        super().__init__()
        self.priority = priority
        self.signatures = signatures
        self.num = num


class ExpressionParseError(Exception):
    "Exception raised for errors during expression parsing"

    def __init__(self, token: Token | None = None, message: str | None = None) -> None:
        if message is not None:
            super().__init__(f"Failed to parse expression: {message}")
            return
        if token is None:
            super().__init__("Failed to parse expression")
            return
        super().__init__(f"Failed to parse on token {token.token_type} with value '{token.value}'")


class AstExpr:
    def __init__(self, expr_type: AstExprType, assignable: bool = False) -> None:
        super().__init__()
        self.type = expr_type
        self.assignable = assignable

    @staticmethod
    def parse(tokenizer: Tokenizer, sym_table: dict[str, AstVariable]) -> AstExpr | None:
        return ExprParser().parse(tokenizer, sym_table)

    def translate(self, translator: Translator) -> list[Instruction | Mark | MarkedInstr]:
        raise NotImplementedError()

    def translate_to_address(self, translator: Translator) -> list[Instruction | Mark | MarkedInstr]:
        addr = self.get_addr(translator)
        if addr is not None:
            return [Instruction(Opcode.LD, addr, ArgType.DIRECT)]
        raise TranslateError(message="Unable to translate_to_address")

    def get_addr(self, translator: Translator) -> int | None:
        return None

    def get_immediate(self, translator: Translator) -> int | None:
        return None

    # returns arg, list of instructions to generate it,
    # and flag that indicates is it necessary to free arg in translator after use
    def translate_to_value_arg(
        self, translator: Translator
    ) -> tuple[tuple[int, ArgType], list[Instruction | Mark | MarkedInstr], bool]:
        res: list[Instruction | Mark | MarkedInstr]
        imm = self.get_immediate(translator)
        if imm is not None:
            return ((imm, ArgType.IMMEDIATE), [], False)

        addr = self.get_addr(translator)
        if addr is not None:
            return ((addr, ArgType.DIRECT), [], False)

        addr = translator.allocate_for_tmp_expr()
        res = self.translate(translator)
        res.append(Instruction(Opcode.ST, addr, ArgType.IMMEDIATE))
        return ((addr, ArgType.DIRECT), res, True)

    def translate_to_address_arg(
        self, translator: Translator
    ) -> tuple[tuple[int, ArgType], list[Instruction | Mark | MarkedInstr], bool]:
        addr = self.get_addr(translator)
        if addr is not None:
            return ((addr, ArgType.IMMEDIATE), [], False)

        addr = translator.allocate_for_tmp_expr()
        res = self.translate_to_address(translator)
        res.append(Instruction(Opcode.ST, addr, ArgType.IMMEDIATE))
        return ((addr, ArgType.INDIRECT), res, True)


class AstExprOperation(AstExpr):
    def __init__(self, op_type: AstOpType, args: list[AstExpr]) -> None:
        super().__init__(
            AstExprOperation._process_type(op_type, args), (op_type == AstOpType.BRACKETS and args[0].assignable)
        )
        self.op_type = op_type
        self.args = args

    def __str__(self) -> str:
        if self.op_type in [AstOpType.UN_MINUS, AstOpType.NOT]:
            return f"({self.op_type.name}) ({self.args[0]})"
        return f"({self.op_type.name}) ({self.args[0]}) ({self.args[1]})"

    @staticmethod
    def _process_type(op_type: AstOpType, args: list[AstExpr]) -> AstExprType:
        for arg_types, res_type in op_type.signatures:
            if len(arg_types) == len(args):
                suitable = True
                for arg, arg_type in zip(args, arg_types):
                    if arg.type != arg_type:
                        suitable = False
                        break
                if suitable:
                    return res_type
        raise ExpressionParseError(
            message=f"Expression type error: there is no suitable signature for {op_type.name}, "
            f"arguments are {[arg.type.name for arg in args]}"
        )

    def translate(self, translator: Translator) -> list[Instruction | Mark | MarkedInstr] | None:
        return ExprTranslator.translate_operation(self, translator)

    def translate_to_address(self, translator: Translator) -> list[Instruction | Mark | MarkedInstr]:
        if self.op_type == AstOpType.BRACKETS and self.args[0].assignable:
            arg, res, to_free = self.args[1].translate_to_value_arg(translator)
            res.extend(self.args[0].translate_to_address(translator))
            res.append(Instruction(Opcode.ADD, arg[0], arg[1]))
            res.append(Instruction(Opcode.ADD, 1, ArgType.IMMEDIATE))
            if to_free:
                translator.free_tmp_expr(arg[0])
            return res
        raise TranslateError(message="Unable to translate_to_address unassignable expression")


class AstInput(AstExpr):
    def __init__(self) -> None:
        super().__init__(AstExprType.CHAR, False)
        self.line: int | None = None

    @staticmethod
    def parse(tokenizer: Tokenizer, sym_table: dict[str, AstVariable]) -> AstInput | None:
        res = AstInput.parse_body(tokenizer, sym_table)
        if not res:
            return None

        token = tokenizer.get_next_token()
        if not token or token.token_type != TokenType.SEMICOLON:
            raise ParseError(token, TokenType.SEMICOLON)

        return res

    @staticmethod
    def parse_body(tokenizer: Tokenizer, sym_table: dict[str, AstVariable]) -> AstInput | None:
        token = tokenizer.get_next_token()
        if not token or token.token_type != TokenType.GETC:
            tokenizer.return_token(token)
            return None

        token = tokenizer.get_next_token()
        if not token or token.token_type != TokenType.L_PAR:
            raise ParseError(token, TokenType.L_PAR)

        token = tokenizer.get_next_token()
        if not token or token.token_type != TokenType.R_PAR:
            raise ParseError(token, TokenType.R_PAR)

        return AstInput()

    def translate(self, translator: Translator) -> list[Instruction | Mark | MarkedInstr]:
        return [Instruction(Opcode.IN)]

    def __str__(self) -> str:
        return f"[Input stmt] at {self.line}"


@enum.unique
class AstLitType(enum.Enum):
    STR = 0
    VAR = 1
    NUM = 2
    CHAR = 3


class AstLit(AstExpr):
    def __init__(self, lit_type: AstLitType, val: str, sym_table: dict[str, AstVariable] | None = None) -> None:
        self.lit_type = lit_type
        match lit_type:
            case AstLitType.NUM:
                self.val = val
                super().__init__(AstExprType.INT)
            case AstLitType.STR:
                self.val = val
                super().__init__(AstExprType.STRING)
            case AstLitType.CHAR:
                self.val = val
                super().__init__(AstExprType.CHAR)
            case AstLitType.VAR:
                assert sym_table is not None
                self.val = val
                try:
                    var = sym_table[val]
                    super().__init__(var.type, True)
                except KeyError as kerror:
                    raise ExpressionParseError(message=f'Unknown variable "{val}"') from kerror

    def translate(self, translator: Translator) -> list[Instruction | Mark | MarkedInstr]:
        match self.lit_type:
            case AstLitType.VAR:
                return [Instruction(Opcode.LD, translator.get_var_addr(self.val), ArgType.DIRECT)]
            case AstLitType.CHAR:
                return [Instruction(Opcode.LD, ord(self.val), ArgType.IMMEDIATE)]
            case AstLitType.STR:
                return [
                    Instruction(
                        Opcode.LD,
                        translator.allocate_data(len(self.val) + 1, self._serialize_string(self.val)),
                        ArgType.IMMEDIATE,
                    )
                ]
            case AstLitType.NUM:
                return [Instruction(Opcode.LD, int(self.val), ArgType.IMMEDIATE)]

    @staticmethod
    def _serialize_string(s: str) -> list[int]:
        return [len(s)] + [ord(c) for c in s]

    def get_addr(self, translator: Translator) -> int | None:
        match self.lit_type:
            case AstLitType.VAR:
                return translator.get_var_addr(self.val)
            case AstLitType.STR:
                return translator.allocate_data(len(self.val) + 1, self._serialize_string(self.val))
            case _:
                return None

    def get_immediate(self, translator: Translator) -> int | None:
        match self.lit_type:
            case AstLitType.NUM:
                return int(self.val)
            case AstLitType.CHAR:
                return ord(self.val)
            case _:
                return None

    def __str__(self) -> str:
        return f"({self.lit_type.name}:{self.type.name}:{self.val})"


class ExprTranslator:
    def __init__(self) -> None:
        pass

    op_to_instr = MappingProxyType(
        {
            AstOpType.BIN_MINUS: Opcode.SUB,
            AstOpType.PLUS: Opcode.ADD,
            AstOpType.MUL: Opcode.MUL,
            AstOpType.DIV: Opcode.DIV,
            AstOpType.MOD: Opcode.REM,
            AstOpType.AND: Opcode.AND,
            AstOpType.OR: Opcode.OR,
        }
    )

    @staticmethod
    def _translate_brackets(
        expr: AstExprOperation, arg: tuple[int, ArgType], translator: Translator
    ) -> list[Instruction]:
        str_addr = expr.args[0].get_addr(translator)

        if str_addr is None:
            raise TranslateError(message="Expected that lhs argument of [] will have addr")

        char_addr = translator.allocate_for_tmp_expr()

        res: list[Instruction] = []

        res = [
            Instruction(Opcode.LD, str_addr, arg_type=ArgType.DIRECT),
            Instruction(Opcode.ADD, arg[0], arg[1]),
            Instruction(Opcode.ADD, 1, arg_type=ArgType.IMMEDIATE),
            Instruction(Opcode.ST, char_addr, arg_type=ArgType.IMMEDIATE),
            Instruction(Opcode.LD, char_addr, arg_type=ArgType.INDIRECT),
        ]

        translator.free_tmp_expr(char_addr)
        return res

    @staticmethod
    def _translate_op(expr: AstExprOperation, arg: tuple[int, ArgType], translator: Translator) -> list[Instruction]:
        if expr.op_type in ExprTranslator.op_to_instr:
            return [Instruction(ExprTranslator.op_to_instr[expr.op_type], arg[0], arg[1])]
        match expr.op_type:
            case AstOpType.EQ | AstOpType.GT | AstOpType.LT | AstOpType.NEQ:
                res = [Instruction(Opcode.CMP, arg[0], arg[1])]
                match expr.op_type:
                    case AstOpType.NEQ:
                        res.append(Instruction(Opcode.SETE))
                        res.append(Instruction(Opcode.NOT))
                    case AstOpType.EQ:
                        res.append(Instruction(Opcode.SETE))
                    case AstOpType.LT:
                        res.append(Instruction(Opcode.SETL))
                    case AstOpType.GT:
                        res.append(Instruction(Opcode.SETG))
                return res
            case _:
                raise TranslateError(message=f"Unexpected operation type {expr.op_type}")

    @staticmethod
    def _translate_bin(expr: AstExprOperation, translator: Translator) -> list[Instruction | Mark | MarkedInstr]:
        res: list[Instruction | Mark | MarkedInstr] = []

        arg, instrs, to_free = expr.args[1].translate_to_value_arg(translator)
        res.extend(instrs)

        if expr.op_type == AstOpType.BRACKETS:
            res.extend(ExprTranslator._translate_brackets(expr, arg, translator))
        else:
            res.extend(expr.args[0].translate(translator))
            res.extend(ExprTranslator._translate_op(expr, arg, translator))

        if to_free:
            translator.free_tmp_expr(arg[0])

        return res

    @staticmethod
    def translate_operation(expr: AstExprOperation, translator: Translator) -> list[Instruction | Mark | MarkedInstr]:
        if len(expr.args) == 1:
            res = expr.args[0].translate(translator)
            opcode = Opcode.NEG if expr.op_type == AstOpType.UN_MINUS else Opcode.NOT
            return [*res, Instruction(opcode)]
        return ExprTranslator._translate_bin(expr, translator)


class ExprParser:
    op_to_ast = MappingProxyType(
        {
            TokenType.PLUS: AstOpType.PLUS,
            TokenType.MINUS: AstOpType.BIN_MINUS,
            TokenType.MUL: AstOpType.MUL,
            TokenType.DIV: AstOpType.DIV,
            TokenType.MOD: AstOpType.MOD,
            TokenType.EQ: AstOpType.EQ,
            TokenType.LT: AstOpType.LT,
            TokenType.GT: AstOpType.GT,
            TokenType.OR: AstOpType.OR,
            TokenType.AND: AstOpType.AND,
            TokenType.L_BRACKET: AstOpType.BRACKETS,
            TokenType.NEQ: AstOpType.NEQ,
        }
    )

    @staticmethod
    def _convert_token_to_ast(token: Token, is_unary: bool) -> AstOpType:
        if is_unary:
            if token.token_type == TokenType.MINUS:
                return AstOpType.UN_MINUS
            if token.token_type == TokenType.NOT:
                return AstOpType.NOT
        return ExprParser.op_to_ast[token.token_type]

    def __init__(self) -> None:
        self.token_stack: list[AstOpType] = []
        self.res_stack: list[AstExpr] = []
        self.is_unary = True
        self.balance = 0

    def place_to_res(self, op: AstOpType) -> bool:
        if op == AstOpType.UN_MINUS or op == AstOpType.NOT:
            arg = self.res_stack.pop()
            self.res_stack.append(AstExprOperation(op, [arg]))
            return True
        arg2 = self.res_stack.pop()
        arg1 = self.res_stack.pop()
        self.res_stack.append(AstExprOperation(op, [arg1, arg2]))
        return True

    def process_op(self, token: Token) -> bool:
        op = ExprParser._convert_token_to_ast(token, self.is_unary)

        while self.token_stack:
            top: AstOpType = self.token_stack.pop()
            if top.priority < op.priority:
                self.token_stack.append(top)
                break
            if not self.place_to_res(top):
                return False
        self.token_stack.append(op)
        return True

    def process_lit(self, token: Token, sym_table: dict[str, AstVariable]) -> bool:
        match token.token_type:
            case TokenType.NUM_LIT:
                self.res_stack.append(AstLit(AstLitType.NUM, token.value))
            case TokenType.STR_LIT:
                self.res_stack.append(AstLit(AstLitType.STR, token.value))
            case TokenType.CHAR_LIT:
                self.res_stack.append(AstLit(AstLitType.CHAR, token.value))
            case TokenType.VAR_LIT:
                self.res_stack.append(AstLit(AstLitType.VAR, token.value, sym_table))
            case _:
                return False
        return True

    def flush_until(self, ast_type: AstOpType) -> bool:
        found = False
        while self.token_stack:
            top = self.token_stack.pop()
            if top == ast_type:
                found = True
                break
            if not self.place_to_res(top):
                return False
        return found

    def process_par(self, token: Token) -> bool:
        if token.token_type == TokenType.L_PAR:
            self.token_stack.append(AstOpType.L_PAR)
            return True

        return self.flush_until(AstOpType.L_PAR)

    def process_bracket(self, token: Token) -> bool:
        if token.token_type == TokenType.L_BRACKET:
            self.token_stack.append(AstOpType.BRACKETS)
            return True

        if not self.flush_until(AstOpType.BRACKETS):
            return False

        return self.place_to_res(AstOpType.BRACKETS)

    def process_getc(
        self,
        token: Token,
        sym_table: dict[str, AstVariable],
        tokenizer: Tokenizer,
    ) -> bool:
        tokenizer.return_token(token)
        inp = AstInput.parse_body(tokenizer, sym_table)
        if not inp:
            return False
        self.res_stack.append(inp)
        return True

    def flush_to_res(self) -> bool:
        while self.token_stack:
            top = self.token_stack.pop()
            if not self.place_to_res(top):
                return False
        return True

    def process_tokens(self, tokenizer: Tokenizer, sym_table: dict[str, AstVariable]):
        while True:
            process_res = True
            token = tokenizer.get_next_token()
            if not token:
                break

            match token.token_type:
                case (
                    TokenType.PLUS
                    | TokenType.MINUS
                    | TokenType.MUL
                    | TokenType.DIV
                    | TokenType.MOD
                    | TokenType.NOT
                    | TokenType.EQ
                    | TokenType.LT
                    | TokenType.GT
                    | TokenType.OR
                    | TokenType.AND
                    | TokenType.NEQ
                ):
                    process_res = self.process_op(token)
                    self.is_unary = True
                case TokenType.NUM_LIT | TokenType.STR_LIT | TokenType.VAR_LIT | TokenType.CHAR_LIT:
                    process_res = self.process_lit(token, sym_table)
                    self.is_unary = False
                case TokenType.R_PAR:
                    if self.balance == 0:
                        tokenizer.return_token(token)
                        break
                    self.balance -= 1
                    process_res = self.process_par(token)
                    self.is_unary = False
                case TokenType.L_PAR:
                    self.balance += 1
                    process_res = self.process_par(token)
                    self.is_unary = True
                case TokenType.R_BRACKET | TokenType.L_BRACKET:
                    process_res = self.process_bracket(token)
                    self.is_unary = token.token_type == TokenType.L_BRACKET
                case TokenType.GETC:
                    process_res = self.process_getc(token, sym_table, tokenizer)
                    self.is_unary = False
                case _:
                    tokenizer.return_token(token)
                    break

            if not process_res:
                raise ExpressionParseError(token)

    def parse(self, tokenizer: Tokenizer, sym_table: dict[str, AstVariable]) -> AstExpr | None:
        self.process_tokens(tokenizer, sym_table)
        self.flush_to_res()
        if len(self.res_stack) > 1:
            raise ExpressionParseError(message="res_stack wrong")
        if len(self.res_stack) == 0:
            return None

        return self.res_stack.pop()


def _set_lines(
    instrs: list[Instruction | Mark | MarkedInstr], line: int | None
) -> list[Instruction | Mark | MarkedInstr]:
    if line is None:
        return instrs
    for i in instrs:
        if isinstance(i, Instruction):
            if i.line is None:
                i.line = line
        if isinstance(i, MarkedInstr):
            if i.instr.line is None:
                i.instr.line = line
    return instrs


def translate(translator: Translator, stmts: list[AstStmt]) -> list[Instruction]:
    res: list[Instruction | Mark | MarkedInstr] = []
    for stmt in stmts:
        stmt_instrs = stmt.translate(translator)
        if stmt_instrs:
            _set_lines(stmt_instrs, stmt.line)
            res.extend(stmt_instrs)
        else:
            raise TranslateError(message="Statement translate error")
    res.append(Instruction(Opcode.HALT))
    return resolve_marks([*translator.preload, *res])


class AstStmt:
    def __init__(self) -> None:
        super().__init__()
        self.line: int | None = None

    @staticmethod
    def parse(tokenizer: Tokenizer, sym_table: dict[str, AstVariable]) -> AstStmt | None:
        token = tokenizer.get_next_token()
        if token is None:
            return None
        line = token.line
        tokenizer.return_token(token)

        for c in [AstInput, AstAssign, AstDecl, AstIf, AstWhile, AstOutput]:
            stmt = c.parse(tokenizer, sym_table)
            if stmt:
                stmt.line = line
                return stmt
        return None

    def translate(self, translator: Translator) -> list[Instruction | Mark | MarkedInstr]:
        raise NotImplementedError()


def _check_next_token(tokenizer: Tokenizer, expected: TokenType) -> Token:
    token = tokenizer.get_next_token()
    if not token or token.token_type != expected:
        raise ParseError(token, expected)
    return token


def _get_next_expr(tokenizer: Tokenizer, sym_table: dict[str, AstVariable]) -> AstExpr:
    try:
        expr = AstExpr.parse(tokenizer, sym_table)
        if not expr:
            raise ParseError(message="Failed to parse: expected expression")
    except ExpressionParseError as ex:
        raise ParseError(message="Failed to parse: expected expression") from ex
    return expr


def _parse_expr_and_stmts_in_braces(
    tokenizer: Tokenizer, sym_table: dict[str, AstVariable]
) -> tuple[AstExpr, list[AstStmt]]:
    _check_next_token(tokenizer, TokenType.L_PAR)

    expr = _get_next_expr(tokenizer, sym_table)
    if expr.type != AstExprType.INT:
        raise ParseError(message=f"Expected condition with result type INT, got {expr.type.name}")

    _check_next_token(tokenizer, TokenType.R_PAR)

    _check_next_token(tokenizer, TokenType.L_BRACE)

    stmt = AstStmt.parse(tokenizer, sym_table)
    stmts: list[AstStmt] = []
    while stmt:
        stmts.append(stmt)
        stmt = AstStmt.parse(tokenizer, sym_table)

    _check_next_token(tokenizer, TokenType.R_BRACE)

    return expr, stmts


class AstIf(AstStmt):
    def __init__(self, expr: AstExpr, statements: list[AstStmt]) -> None:
        super().__init__()
        self.expr = expr
        self.statements = statements

    @staticmethod
    def parse(tokenizer: Tokenizer, sym_table: dict[str, AstVariable]) -> AstIf | None:
        token = tokenizer.get_next_token()
        if not token or token.token_type != TokenType.IF:
            tokenizer.return_token(token)
            return None

        return AstIf(*_parse_expr_and_stmts_in_braces(tokenizer, sym_table))

    def translate(self, translator: Translator) -> list[Instruction | Mark | MarkedInstr]:
        res = self.expr.translate(translator)

        mark = Mark()

        res.append(MarkedInstr(Instruction(Opcode.JZ, -1, ArgType.IMMEDIATE), mark))
        for s in self.statements:
            res.extend(_set_lines(s.translate(translator), s.line))
        res.append(mark)

        return res

    def __str__(self) -> str:
        newline = "\n"
        return f"If statement: [expr: {self.expr}] at {self.line}\n[statements: {newline.join(str(i) for i in self.statements)}]"


class AstWhile(AstStmt):
    def __init__(self, expr: AstExpr, statements: list[AstStmt]) -> None:
        super().__init__()
        self.expr = expr
        self.statements = statements

    @staticmethod
    def parse(tokenizer: Tokenizer, sym_table: dict[str, AstVariable]) -> AstWhile | None:
        token = tokenizer.get_next_token()
        if not token or token.token_type != TokenType.WHILE:
            tokenizer.return_token(token)
            return None

        return AstWhile(*_parse_expr_and_stmts_in_braces(tokenizer, sym_table))

    def translate(self, translator: Translator) -> list[Instruction | Mark | MarkedInstr]:
        res: list[Instruction | Mark | MarkedInstr] = []

        mark_loop = Mark()
        mark_after = Mark()

        res.append(mark_loop)

        res.extend(self.expr.translate(translator))

        res.append(MarkedInstr(Instruction(Opcode.JZ, -1, ArgType.IMMEDIATE), mark_after))
        for s in self.statements:
            res.extend(_set_lines(s.translate(translator), s.line))
        res.append(MarkedInstr(Instruction(Opcode.JMP, -1, ArgType.IMMEDIATE), mark_loop))
        res.append(mark_after)

        return res

    def __str__(self) -> str:
        newline = "\n"
        return f"While statement: [expr: {self.expr}] at {self.line}\nstatements:\n{newline.join(str(i) for i in self.statements)}\nend_of_statements"


class AstOutput(AstStmt):
    def __init__(self, expr: AstExpr) -> None:
        super().__init__()
        self.expr = expr

    @staticmethod
    def parse(tokenizer: Tokenizer, sym_table: dict[str, AstVariable]) -> AstOutput | None:
        token = tokenizer.get_next_token()
        if not token or token.token_type != TokenType.PRINT:
            tokenizer.return_token(token)
            return None

        token = _check_next_token(tokenizer, TokenType.L_PAR)

        expr = _get_next_expr(tokenizer, sym_table)
        if expr.type != AstExprType.CHAR and expr.type != AstExprType.STRING:
            raise ParseError(message="Wrong type! Expected CHAR or STRING expression type as print() argument")

        token = _check_next_token(tokenizer, TokenType.R_PAR)

        _check_next_token(tokenizer, TokenType.SEMICOLON)

        return AstOutput(expr)

    def translate(self, translator: Translator) -> list[Instruction | Mark | MarkedInstr]:
        res = self.expr.translate(translator)
        if self.expr.type == AstExprType.CHAR:
            res.append(Instruction(Opcode.OUT))
            return res

        read = translator.allocate_for_tmp_expr()
        addr = translator.allocate_for_tmp_expr()
        cnt = translator.allocate_for_tmp_expr()

        loop = Mark()

        res.append(Instruction(Opcode.ST, addr, ArgType.IMMEDIATE))
        res.append(Instruction(Opcode.LD, addr, ArgType.INDIRECT))

        res.append(loop)
        res.append(Instruction(Opcode.ST, cnt, ArgType.IMMEDIATE))

        res.append(Instruction(Opcode.LD, addr, ArgType.DIRECT))
        res.append(Instruction(Opcode.ADD, 1, ArgType.IMMEDIATE))
        res.append(Instruction(Opcode.ADD, addr, ArgType.INDIRECT))
        res.append(Instruction(Opcode.SUB, cnt, ArgType.DIRECT))

        res.append(Instruction(Opcode.ST, read, ArgType.IMMEDIATE))
        res.append(Instruction(Opcode.LD, read, ArgType.INDIRECT))

        res.append(Instruction(Opcode.OUT))

        res.append(Instruction(Opcode.LD, cnt, ArgType.DIRECT))
        res.append(Instruction(Opcode.SUB, 1, ArgType.IMMEDIATE))
        res.append(MarkedInstr(Instruction(Opcode.JNZ, -1, ArgType.IMMEDIATE), loop))

        return res

    def __str__(self) -> str:
        return f"Output stmt: [expr {self.expr}] at {self.line}"


class AstDecl(AstStmt):
    def __init__(
        self, var_type: AstExprType, var_name: str, expr: AstExpr | None = None, str_len: int | None = None
    ) -> None:
        super().__init__()
        self.var_type = var_type
        self.str_len = str_len
        self.var_name = var_name
        self.expr = expr

    @staticmethod
    def parse(tokenizer: Tokenizer, sym_table: dict[str, AstVariable]) -> AstDecl | None:
        decl = AstDecl._parse_decl(tokenizer, sym_table)
        if not decl:
            return None
        if decl.var_name in sym_table:
            raise ParseError(message=f"Variable {decl.var_name} already exists")
        sym_table[decl.var_name] = AstVariable(decl.var_type, decl.var_name)
        return decl

    @staticmethod
    def _parse_decl(tokenizer: Tokenizer, sym_table: dict[str, AstVariable]) -> AstDecl | None:
        token = tokenizer.get_next_token()
        if not token:
            tokenizer.return_token(token)
            return None

        match token.token_type:
            case TokenType.INT:
                var_type = AstExprType.INT
            case TokenType.CHAR:
                var_type = AstExprType.CHAR
            case TokenType.STRING:
                var_type = AstExprType.STRING
            case _:
                tokenizer.return_token(token)
                return None

        token = _check_next_token(tokenizer, TokenType.VAR_LIT)

        var_name = token.value
        token = tokenizer.get_next_token()
        if not token or token.token_type != TokenType.ASSIGN:
            str_len = None
            if var_type == AstExprType.STRING and token and token.token_type == TokenType.L_BRACKET:
                token = _check_next_token(tokenizer, TokenType.NUM_LIT)
                str_len = int(token.value)
                token = _check_next_token(tokenizer, TokenType.R_BRACKET)
                token = tokenizer.get_next_token()
            if not token or token.token_type != TokenType.SEMICOLON:
                raise ParseError(token, TokenType.SEMICOLON)
            return AstDecl(var_type, var_name, str_len=str_len)

        expr = _get_next_expr(tokenizer, sym_table)
        if expr.type != var_type:
            raise ParseError(message=f"Expected expr with type {var_type.name}, got {expr.type.name}")

        _check_next_token(tokenizer, TokenType.SEMICOLON)

        return AstDecl(var_type, var_name, expr)

    def translate(self, translator: Translator) -> list[Instruction | Mark | MarkedInstr]:
        var_addr = translator.allocate_var(self.var_name)
        res: list[Instruction | Mark | MarkedInstr] = []
        if self.str_len is not None:
            res = [
                Instruction(Opcode.LD, translator.allocate_data(self.str_len + 1, [self.str_len]), ArgType.IMMEDIATE)
            ]
        elif self.expr is not None:
            res = self.expr.translate(translator)
        else:
            res = [Instruction(Opcode.LD, 0, ArgType.IMMEDIATE)]
        if len(res) == 0:
            return []
        res.append(Instruction(Opcode.ST, var_addr, ArgType.IMMEDIATE))
        return res

    def __str__(self) -> str:
        return (
            f"Decl statements: [var_type {self.var_type.name}{' ' + str(self.str_len) if self.str_len else ''}]"
            f"[var_name {self.var_name}] [expr {self.expr}] at {self.line}"
        )


class AstAssign(AstStmt):
    def __init__(self, dst: AstExpr, expr: AstExpr) -> None:
        super().__init__()
        self.dst = dst
        self.expr = expr

    @staticmethod
    def parse(tokenizer: Tokenizer, sym_table: dict[str, AstVariable]) -> AstAssign | None:
        dst_expr = AstExpr.parse(tokenizer, sym_table)
        if not dst_expr:
            return None

        if not dst_expr.assignable:
            raise ParseError(message="Unable to assign value to this expression")

        _check_next_token(tokenizer, TokenType.ASSIGN)

        expr = _get_next_expr(tokenizer, sym_table)

        if dst_expr.type != expr.type:
            raise ParseError(message=f"Unable to assign type {expr.type.name} to {dst_expr.type.name}")

        _check_next_token(tokenizer, TokenType.SEMICOLON)

        return AstAssign(dst_expr, expr)

    def translate(self, translator: Translator) -> list[Instruction | Mark | MarkedInstr]:
        dst_arg, res, to_free = self.dst.translate_to_address_arg(translator)

        res.extend(self.expr.translate(translator))

        res.append(Instruction(Opcode.ST, dst_arg[0], dst_arg[1]))

        if to_free:
            translator.free_tmp_expr(dst_arg[0])

        return res

    def __str__(self) -> str:
        return f"Assign statements: [dst {self.dst}] [expr {self.expr}] at {self.line}"


class Parser:
    def __init__(self, text: TextIOBase) -> None:
        self.statements: list[AstStmt] = []
        self.tokenizer = Tokenizer(text)
        self.sym_table: dict[str, AstVariable] = dict()

    def parse(self) -> None:
        while self.get_next_stmt():
            pass

    def get_next_stmt(self) -> AstStmt | None:
        stmt = AstStmt.parse(self.tokenizer, self.sym_table)
        if stmt:
            self.statements.append(stmt)
        return stmt


def main(src: str, dst: str, verbose: bool):
    with open(src) as source:
        parser = Parser(source)
        parser.parse()

        translator = Translator()
        res = translate(translator, parser.statements)

        with open(dst, mode="w+") as destination:
            if verbose:
                source.seek(0, io.SEEK_SET)
                lines = source.readlines()
                for line, instrs_in_line in groupby(enumerate(res), lambda x: x[1].line):
                    if line is None:
                        print("No line:", file=destination)
                    else:
                        print(f"{lines[line].strip()}:", file=destination)
                    for n, instr in instrs_in_line:
                        print(f"\t{n:<4} {instr}", file=destination)
            else:
                for n, instr in enumerate(res):
                    print(f"\t{n:<4} {instr}", file=destination)


if __name__ == "__main__":
    args = sys.argv
    src = args[1]
    dst = args[2]
    verbose = False

    if len(args) == 4:
        if args[3] == "-v":
            verbose = True
        else:
            raise ValueError()

    main(src, dst, verbose)
