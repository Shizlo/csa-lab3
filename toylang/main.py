from __future__ import annotations

import logging
import sys

from toylang.isa import Instruction
from toylang.machine import ControlUnit, DataPath


def parse_isa(lines: list[str]) -> list[Instruction]:
    res: list[Instruction] = []
    for line in lines:
        try:
            res.append(Instruction.parse(line.split(maxsplit=1)[1]))
        except ValueError:
            pass
        except IndexError:
            pass
    return res


def main(code_file: str, input_file: str):
    with open(code_file) as code:
        program = parse_isa(code.readlines())
    data_path = DataPath(100000000, input_file)
    control_unit = ControlUnit(program, data_path)
    try:
        while True:
            control_unit.process_instr()
    except StopIteration:
        logging.debug("Program terminated")
    data_path.device.out_buffer()


if __name__ == "__main__":
    if len(sys.argv) == 4:
        logging.basicConfig(filename=sys.argv[3], level=logging.DEBUG, format="%(message)s")
    main(sys.argv[1], "../input.txt")
